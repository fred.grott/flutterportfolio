![task slice](media/task_slice_thumb.png)
# FlutterPortfolio
This a repo showing off my flutter mobile demos and how I design and develop mobile apps targeting ios and android using Google's Flutter Front-End Framework.
# What Problems Cam A FrontEnd App Solve?
The myth is that a front-end app design solve getting potential users to discover the app in the noisy marketplace of attention. Unfortunately no it cannot! [See the Grammary example in this post](https://victoreduoh.com/grammarlys-marketing-strategy/?utm_source=angellist) for how to use targeted and focused micro-content and social media to relly solve the app discovery problem. What the design of the front-end app solves is how to possibly convert the hghest amount of first time users to daily and monthly users. I purposedly avoid project gigs where the stakeholders have the mobile app will fix app discovering expectations unless the firm wantas to pay for an extra package where I do the 18 months of markeing work to fix app discovery.
# Background
This can be considered my creditability part. I am just transforming my previous android native-java-kotlin startup experience to perfecting flutter oop and flutter designed apps
## Current Contributions to Flutter
I contribute to the Flutter Community of plugins such as:
### Flutter Platform Widgets at [Flutter Platform Widgets at github](https://github.com/aqwert/flutter_platform_widgets)
This pluginn automates a theme-atic way of dynamically delivering Material widgets to android, desktop-windows, and web and Cupertino widgets to ios and desktop-macosx. My controibutions thus far is cleaning up the examples of using the plugin so that they work without app stoping errors as the project peopple-resources is quite small will only the project lead and few others as part time contributors.
![references](media/references.jpg)
## References
Several people know of my mobile expertise.
### Jerry Schuman, Founder Fliite
Jerry is a serial startup founder that has 9 startups to his career and has a very good feel on detecting developer experts. His achievements can be viewed on his [LinkedIN Profile](https://www.linkedin.com/in/jerryschuman/) and his gmail addy is jerry DOT schuman AT gmail DOT com. Note, that I am unoffically involved in exchanging expertise with an unofficial team focused arouond Fiite via Jerry Schuman.
![project evaluation](media/projectevaluation.jpg)
# Project Evaluation, The First Step
The first step in seeing if I will build your mobile app is to participate in my project evaluation process. Its in your best interest to participate as you get some helpful free advice of things that should be in the spec and part of the project, etc.
### NDA Signing
Due to the app brand, code, visual design, etc being your IP we need to get a NDA that is signed by both of us so that I can review the spec you may have created under the terms of the NDA.  This cannot be done by phone and I really do not take phone calls at this point as you at this point do not have my on  retainer and my retainer packages cover so many hours answering questiosn by phone.
### Reviewing Your Mobile Spec
Your mobile app spec generally covers the features of the app, your expectations, and of course such things as what the mobile app is being formulated to solve as far as problems. If you do not have a spec for your proposed mobile app, I do offer paid spec writing packages that involve interviewing you and writing the spec. You can see teh spec template I use at the root of this repo. My review of your mobile app spec coves not only feasiblity but also the soft areas of will it meet all your expectations.
### Discussing and Writing Contract Terms
No, still no phone calls as that is saved for my clients that pay my retainer for the retainer-phone-support package. I generally will start a contract terms discussion pointing out the expected ROI of the project and whether my cost model fits into the ROI. If it does not, not to worry you still keepp all the free advice including any spec that I may have modified or written, etc. AND of course, part of the discussion of expected ROI is the real-lifebiz-fact that there will be a paid advance on a signed contract. you can get an idea of the things I include in a contract by the contract tempplate at the root of this git repo project.

![mobile app demos](media/app_demo.jpg)
# Mobile App Demos
This is a sampling of my public flutter moobile app demos.
## Task Slice at [task slice at gitlab](https://gitlab.com/fred.grott/task_slice)
Just a simple task-pomodoro-timing flutter mobile app. Supports dark mode and adheres to Apple flat design UI guidelines on ios and Google's Material Design UI guidelines for android.
### Vids
[![task slice](https://i.ytimg.com/vi/0meTH1WCiHQ/hqdefault.jpg?sqp=-oaymwEZCNACELwBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAViP4SDcCS73Bxw1nDXKjLWTEIiw)](https://www.youtube.com/watch?v=0meTH1WCiHQ&t=9s)
## Githuber[githuber at gitlab repo]()
Githuber is a github client to explore a users stuff on github using the Github graphQL extension to their online api. GraphQL is important becuase databases are moving form nosql to the next step a knowledge graph way of doing data querying as it matches up wth humkan lnaguages, etc that is now being reharnessed in mobile apps.
### Vids
# How I Work, Design, Development, And Remote
Well, you already know that I prioritize stuff and that my phone suport answering questions if for those clients paying for my retainer-phone-support-package-plan. This done because for every client project I meed the project deadline with some time breathing room as I plan for the unexpected to deliver full no-excuse project contracted mobile app development and design. Here is some of the standaards I follow both in the design area and development areas.
## UX(CX) Design
Its the users(consumer) experience of using the mobile app connected to whatever platform your are transforming to mobile. The current trends of UX are moving towards using a motion UX to brand mobile apps. Its not exactly digital as its happens in the confined mobile screen space but is animated. While screen transitions are still standardized per UI guildiens per paltform, animating a UI node transtion from one UI node to the another UI node on the screen is where I create cusotmized animation to brand your mobile app with the goal of focusing and than un-focusing user attention which results in completing of an app task.
I also make use of free UX research from the [Norman Nielsen Group](https://www.nngroup.com/) who are experts on both UX and numan user interface design.
## Mobile App Development
With my native-java-kotlin-android-startup expereince I have very opionated view of OOP in that I know what works and what should be shunned for a better solution that both matches short-term-benefits with long-term-benefits. In short, Google is somewhat wrong in their advice to flutter app devs and designers to use the Inherited-Widget-Provider-Dependency-Injection combination. The correct solution that even gives correct SEo-style urls for web apps and correct injection without forching devs to resort ot singletons is a stateful-widget-service-locator-dependency-inject so that everything is fully testable which than of course requires less mocking in instrumented testing. The plugin I use for that solution is [Modular](https://github.com/Flutterando/modular). I plan on learning a little Portuguesemij order to fully contribute to this project that started as a Brazilian Flutter commmunity project.
## Remote Working
Everyone has heard of the coronavirus-19 outbreaks. I work remote form such places as my local Purdue Univeristy Northwest at Westville wifi-cafe part of their public library. Fiber-internet speeds with no co-work space fees and pleasant environment. And of course, as planned I have other wifi-places to use if needed. I refrain from using any VPN as that is not allowed when submitting to the mobile app stores. I use a secre and passwrod protected layptop with encrypted drives as protection of your IP assets is my top priority.
# The Service Plans I Offer
The service plans I offer are:
* Paid Retainer And Phone Suport Package
* Paid Project Spec Writing and Mobile Feasiblity Review Package
* Mobile App Project Design and Development Package
* Paid Microcontent marketing to fix App Discovery Package

Please note that I have not yet broken out specific mobile app packages such as one specializing on the firebase-framelink-db-csm combination with hybrid web app for example. If that is your need than ask.
# Contacting Me
I can be contacted at my gmail addy of fred DOT grott AT gmail DOT com. I can also be followed on social media. For those with a developer spirit I can be found on [github](https://github.com/fredgrott),[gitlab](https://gitlab.com/fred.grott), and [keybase](https://keybase.io/fredgrott). My design social profiles are [Behance](https://www.behance.net/gwsfredgrott) and [Dribbble](https://dribbble.com/FredGrott). While the main social-style social media platforms I interact on are [twitter](https://twitter.com/fredgrott),[linkedin](https://www.linkedin.com/in/fredgrottstartupfluttermobileappdesigner/), and [xing](https://www.xing.com/profile/Fred_Grott/cv).
